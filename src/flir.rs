use ndarray::*;
use std::path::{Path, PathBuf};
use flyr::try_parse_flir;

use crate::ThermogramTrait;

/// This is the struct and `ThermogramTrait` implementation for FLIR thermograms, using
/// [flyr](https://crates.io/crates/flyr).
///
/// While a file can be directly read with `from_file`, it is recommended to instead use the
/// `Thermogram::from_file` instead. The latter detects what kind of file (TIFF, FLIR) it is dealing
/// with, subsequently choosing the right reader for it. This way your application support different
/// thermogram formats.
#[derive(Clone, Debug)]
pub struct FlirThermogram {
    thermal: Array<f32, Ix2>,
    file_path: PathBuf,
}

impl FlirThermogram {
    /// Read a FLIR file referenced by a path.
    ///
    /// # Arguments
    /// * `file_path` - The path to the FLIR file to read.
    ///
    /// # Returns
    /// In case of success, `Some<FlirThermogram>` is returned, otherwise `None`. Values are in
    /// centigrades, as specified by the `ThermogramTrait` contract.
    pub fn from_file(file_path: &Path) -> Option<FlirThermogram> {
        let thermogram = FlirThermogram::read_thermal(file_path);
        thermogram
    }

    fn read_thermal(file_path: &Path) -> Option<FlirThermogram> {
        let r_kelvin = try_parse_flir(file_path);
        match r_kelvin {
            Ok(kelvin) => Some(FlirThermogram {
                thermal: kelvin - 275.15,
                file_path: (*file_path).to_path_buf(),
            }),
            _ => None,
        }
    }
}

impl ThermogramTrait for FlirThermogram {
    fn thermal(&self) -> Array<f32, Ix2> {
        self.thermal.clone()
    }

    fn optical(&self) -> Option<&Array<u8, Ix3>> {
        None
    }

    fn identifier(&self) -> &str {
        // FIXME unwraps
        let file_name = self.file_path.file_name();
        file_name.unwrap().to_str().unwrap()
    }

    fn path(&self) -> Option<&str> {
        Some(self.file_path.to_str().unwrap())
    }
}

impl From<&FlirThermogram> for Array<f32, Ix2> {
    fn from(thermogram: &FlirThermogram) -> Array<f32, Ix2> {
        thermogram.thermal()
    }
}

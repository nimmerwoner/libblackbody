pub const ORANGES: [[f32; 3]; 256] = [
    [1.000, 0.961, 0.922],
    [1.000, 0.959, 0.918],
    [1.000, 0.957, 0.914],
    [1.000, 0.955, 0.911],
    [1.000, 0.953, 0.907],
    [0.999, 0.952, 0.904],
    [0.999, 0.950, 0.900],
    [0.999, 0.948, 0.897],
    [0.999, 0.946, 0.893],
    [0.999, 0.944, 0.889],
    [0.999, 0.942, 0.886],
    [0.999, 0.940, 0.882],
    [0.999, 0.939, 0.879],
    [0.998, 0.937, 0.875],
    [0.998, 0.935, 0.872],
    [0.998, 0.933, 0.868],
    [0.998, 0.931, 0.864],
    [0.998, 0.929, 0.861],
    [0.998, 0.928, 0.857],
    [0.998, 0.926, 0.854],
    [0.998, 0.924, 0.850],
    [0.997, 0.922, 0.847],
    [0.997, 0.920, 0.843],
    [0.997, 0.918, 0.840],
    [0.997, 0.916, 0.836],
    [0.997, 0.915, 0.832],
    [0.997, 0.913, 0.829],
    [0.997, 0.911, 0.825],
    [0.997, 0.909, 0.822],
    [0.996, 0.907, 0.818],
    [0.996, 0.905, 0.815],
    [0.996, 0.904, 0.811],
    [0.996, 0.902, 0.807],
    [0.996, 0.899, 0.802],
    [0.996, 0.896, 0.796],
    [0.996, 0.894, 0.791],
    [0.996, 0.891, 0.786],
    [0.995, 0.888, 0.780],
    [0.995, 0.885, 0.775],
    [0.995, 0.883, 0.769],
    [0.995, 0.880, 0.764],
    [0.995, 0.877, 0.758],
    [0.995, 0.875, 0.753],
    [0.995, 0.872, 0.748],
    [0.995, 0.869, 0.742],
    [0.994, 0.866, 0.737],
    [0.994, 0.864, 0.731],
    [0.994, 0.861, 0.726],
    [0.994, 0.858, 0.721],
    [0.994, 0.856, 0.715],
    [0.994, 0.853, 0.710],
    [0.994, 0.850, 0.704],
    [0.994, 0.847, 0.699],
    [0.993, 0.845, 0.693],
    [0.993, 0.842, 0.688],
    [0.993, 0.839, 0.683],
    [0.993, 0.837, 0.677],
    [0.993, 0.834, 0.672],
    [0.993, 0.831, 0.666],
    [0.993, 0.829, 0.661],
    [0.993, 0.826, 0.656],
    [0.992, 0.823, 0.650],
    [0.992, 0.820, 0.645],
    [0.992, 0.818, 0.639],
    [0.992, 0.815, 0.634],
    [0.992, 0.810, 0.627],
    [0.992, 0.806, 0.620],
    [0.992, 0.802, 0.613],
    [0.992, 0.798, 0.607],
    [0.992, 0.794, 0.600],
    [0.992, 0.790, 0.593],
    [0.992, 0.785, 0.586],
    [0.992, 0.781, 0.579],
    [0.992, 0.777, 0.573],
    [0.992, 0.773, 0.566],
    [0.992, 0.769, 0.559],
    [0.992, 0.764, 0.552],
    [0.992, 0.760, 0.546],
    [0.992, 0.756, 0.539],
    [0.992, 0.752, 0.532],
    [0.992, 0.748, 0.525],
    [0.992, 0.744, 0.519],
    [0.992, 0.739, 0.512],
    [0.992, 0.735, 0.505],
    [0.992, 0.731, 0.498],
    [0.992, 0.727, 0.492],
    [0.992, 0.723, 0.485],
    [0.992, 0.718, 0.478],
    [0.992, 0.714, 0.471],
    [0.992, 0.710, 0.464],
    [0.992, 0.706, 0.458],
    [0.992, 0.702, 0.451],
    [0.992, 0.698, 0.444],
    [0.992, 0.693, 0.437],
    [0.992, 0.689, 0.431],
    [0.992, 0.685, 0.424],
    [0.992, 0.681, 0.417],
    [0.992, 0.677, 0.412],
    [0.992, 0.673, 0.406],
    [0.992, 0.669, 0.400],
    [0.992, 0.665, 0.394],
    [0.992, 0.661, 0.389],
    [0.992, 0.656, 0.383],
    [0.992, 0.652, 0.377],
    [0.992, 0.648, 0.371],
    [0.992, 0.644, 0.365],
    [0.992, 0.640, 0.360],
    [0.992, 0.636, 0.354],
    [0.992, 0.632, 0.348],
    [0.992, 0.628, 0.342],
    [0.992, 0.624, 0.336],
    [0.992, 0.620, 0.331],
    [0.992, 0.616, 0.325],
    [0.992, 0.612, 0.319],
    [0.992, 0.608, 0.313],
    [0.992, 0.604, 0.308],
    [0.992, 0.600, 0.302],
    [0.992, 0.596, 0.296],
    [0.992, 0.592, 0.290],
    [0.992, 0.587, 0.284],
    [0.992, 0.583, 0.279],
    [0.992, 0.579, 0.273],
    [0.992, 0.575, 0.267],
    [0.992, 0.571, 0.261],
    [0.992, 0.567, 0.256],
    [0.992, 0.563, 0.250],
    [0.992, 0.559, 0.244],
    [0.992, 0.555, 0.238],
    [0.991, 0.551, 0.233],
    [0.990, 0.546, 0.228],
    [0.988, 0.542, 0.223],
    [0.987, 0.537, 0.218],
    [0.986, 0.533, 0.213],
    [0.984, 0.529, 0.208],
    [0.983, 0.524, 0.203],
    [0.981, 0.520, 0.197],
    [0.980, 0.515, 0.192],
    [0.978, 0.511, 0.187],
    [0.977, 0.506, 0.182],
    [0.975, 0.502, 0.177],
    [0.974, 0.498, 0.172],
    [0.972, 0.493, 0.167],
    [0.971, 0.489, 0.162],
    [0.969, 0.484, 0.157],
    [0.968, 0.480, 0.152],
    [0.966, 0.475, 0.147],
    [0.965, 0.471, 0.142],
    [0.963, 0.467, 0.137],
    [0.962, 0.462, 0.132],
    [0.960, 0.458, 0.127],
    [0.959, 0.453, 0.122],
    [0.957, 0.449, 0.117],
    [0.956, 0.444, 0.112],
    [0.955, 0.440, 0.107],
    [0.953, 0.436, 0.102],
    [0.952, 0.431, 0.097],
    [0.950, 0.427, 0.092],
    [0.949, 0.422, 0.086],
    [0.947, 0.418, 0.081],
    [0.946, 0.413, 0.076],
    [0.943, 0.409, 0.073],
    [0.940, 0.405, 0.071],
    [0.937, 0.401, 0.069],
    [0.934, 0.397, 0.066],
    [0.931, 0.393, 0.064],
    [0.928, 0.389, 0.062],
    [0.926, 0.385, 0.060],
    [0.923, 0.381, 0.058],
    [0.920, 0.377, 0.055],
    [0.917, 0.373, 0.053],
    [0.914, 0.369, 0.051],
    [0.911, 0.365, 0.049],
    [0.908, 0.361, 0.047],
    [0.905, 0.356, 0.044],
    [0.902, 0.352, 0.042],
    [0.899, 0.348, 0.040],
    [0.896, 0.344, 0.038],
    [0.893, 0.340, 0.035],
    [0.890, 0.336, 0.033],
    [0.887, 0.332, 0.031],
    [0.884, 0.328, 0.029],
    [0.881, 0.324, 0.027],
    [0.878, 0.320, 0.024],
    [0.875, 0.316, 0.022],
    [0.872, 0.312, 0.020],
    [0.869, 0.308, 0.018],
    [0.866, 0.304, 0.016],
    [0.864, 0.300, 0.013],
    [0.861, 0.296, 0.011],
    [0.858, 0.291, 0.009],
    [0.855, 0.287, 0.007],
    [0.852, 0.283, 0.004],
    [0.846, 0.281, 0.004],
    [0.840, 0.278, 0.004],
    [0.834, 0.276, 0.005],
    [0.827, 0.274, 0.005],
    [0.821, 0.272, 0.005],
    [0.815, 0.270, 0.005],
    [0.809, 0.267, 0.006],
    [0.802, 0.265, 0.006],
    [0.796, 0.263, 0.006],
    [0.790, 0.261, 0.006],
    [0.784, 0.259, 0.007],
    [0.777, 0.256, 0.007],
    [0.771, 0.254, 0.007],
    [0.765, 0.252, 0.007],
    [0.758, 0.250, 0.008],
    [0.752, 0.247, 0.008],
    [0.746, 0.245, 0.008],
    [0.740, 0.243, 0.008],
    [0.733, 0.241, 0.009],
    [0.727, 0.239, 0.009],
    [0.721, 0.236, 0.009],
    [0.715, 0.234, 0.009],
    [0.708, 0.232, 0.010],
    [0.702, 0.230, 0.010],
    [0.696, 0.228, 0.010],
    [0.689, 0.225, 0.010],
    [0.683, 0.223, 0.011],
    [0.677, 0.221, 0.011],
    [0.671, 0.219, 0.011],
    [0.664, 0.216, 0.011],
    [0.658, 0.214, 0.011],
    [0.652, 0.212, 0.012],
    [0.647, 0.210, 0.012],
    [0.642, 0.208, 0.012],
    [0.637, 0.206, 0.012],
    [0.632, 0.205, 0.012],
    [0.628, 0.203, 0.012],
    [0.623, 0.201, 0.012],
    [0.618, 0.199, 0.013],
    [0.613, 0.197, 0.013],
    [0.608, 0.195, 0.013],
    [0.604, 0.194, 0.013],
    [0.599, 0.192, 0.013],
    [0.594, 0.190, 0.013],
    [0.589, 0.188, 0.013],
    [0.584, 0.186, 0.013],
    [0.580, 0.184, 0.014],
    [0.575, 0.182, 0.014],
    [0.570, 0.181, 0.014],
    [0.565, 0.179, 0.014],
    [0.560, 0.177, 0.014],
    [0.556, 0.175, 0.014],
    [0.551, 0.173, 0.014],
    [0.546, 0.171, 0.014],
    [0.541, 0.170, 0.015],
    [0.536, 0.168, 0.015],
    [0.532, 0.166, 0.015],
    [0.527, 0.164, 0.015],
    [0.522, 0.162, 0.015],
    [0.517, 0.160, 0.015],
    [0.512, 0.158, 0.015],
    [0.508, 0.157, 0.015],
    [0.503, 0.155, 0.016],
    [0.498, 0.153, 0.016],
];

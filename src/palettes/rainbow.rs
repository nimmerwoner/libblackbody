pub const RAINBOW: [[f32; 3]; 256] = [
    [0.500, 0.000, 1.000],
    [0.492, 0.012, 1.000],
    [0.484, 0.025, 1.000],
    [0.476, 0.037, 1.000],
    [0.469, 0.049, 1.000],
    [0.461, 0.062, 1.000],
    [0.453, 0.074, 0.999],
    [0.445, 0.086, 0.999],
    [0.437, 0.098, 0.999],
    [0.429, 0.111, 0.998],
    [0.422, 0.123, 0.998],
    [0.414, 0.135, 0.998],
    [0.406, 0.147, 0.997],
    [0.398, 0.159, 0.997],
    [0.390, 0.172, 0.996],
    [0.382, 0.184, 0.996],
    [0.375, 0.196, 0.995],
    [0.367, 0.208, 0.995],
    [0.359, 0.220, 0.994],
    [0.351, 0.232, 0.993],
    [0.343, 0.244, 0.992],
    [0.335, 0.256, 0.992],
    [0.327, 0.268, 0.991],
    [0.320, 0.280, 0.990],
    [0.312, 0.291, 0.989],
    [0.304, 0.303, 0.988],
    [0.296, 0.315, 0.987],
    [0.288, 0.327, 0.986],
    [0.280, 0.338, 0.985],
    [0.273, 0.350, 0.984],
    [0.265, 0.361, 0.983],
    [0.257, 0.373, 0.982],
    [0.249, 0.384, 0.981],
    [0.241, 0.395, 0.979],
    [0.233, 0.407, 0.978],
    [0.225, 0.418, 0.977],
    [0.218, 0.429, 0.976],
    [0.210, 0.440, 0.974],
    [0.202, 0.451, 0.973],
    [0.194, 0.462, 0.971],
    [0.186, 0.473, 0.970],
    [0.178, 0.484, 0.968],
    [0.171, 0.495, 0.967],
    [0.163, 0.505, 0.965],
    [0.155, 0.516, 0.963],
    [0.147, 0.526, 0.962],
    [0.139, 0.537, 0.960],
    [0.131, 0.547, 0.958],
    [0.124, 0.557, 0.957],
    [0.116, 0.568, 0.955],
    [0.108, 0.578, 0.953],
    [0.100, 0.588, 0.951],
    [0.092, 0.598, 0.949],
    [0.084, 0.608, 0.947],
    [0.076, 0.617, 0.945],
    [0.069, 0.627, 0.943],
    [0.061, 0.636, 0.941],
    [0.053, 0.646, 0.939],
    [0.045, 0.655, 0.937],
    [0.037, 0.665, 0.935],
    [0.029, 0.674, 0.932],
    [0.022, 0.683, 0.930],
    [0.014, 0.692, 0.928],
    [0.006, 0.701, 0.926],
    [0.002, 0.709, 0.923],
    [0.010, 0.718, 0.921],
    [0.018, 0.726, 0.918],
    [0.025, 0.735, 0.916],
    [0.033, 0.743, 0.914],
    [0.041, 0.751, 0.911],
    [0.049, 0.759, 0.908],
    [0.057, 0.767, 0.906],
    [0.065, 0.775, 0.903],
    [0.073, 0.783, 0.901],
    [0.080, 0.791, 0.898],
    [0.088, 0.798, 0.895],
    [0.096, 0.805, 0.892],
    [0.104, 0.813, 0.890],
    [0.112, 0.820, 0.887],
    [0.120, 0.827, 0.884],
    [0.127, 0.834, 0.881],
    [0.135, 0.840, 0.878],
    [0.143, 0.847, 0.875],
    [0.151, 0.853, 0.872],
    [0.159, 0.860, 0.869],
    [0.167, 0.866, 0.866],
    [0.175, 0.872, 0.863],
    [0.182, 0.878, 0.860],
    [0.190, 0.884, 0.857],
    [0.198, 0.890, 0.853],
    [0.206, 0.895, 0.850],
    [0.214, 0.901, 0.847],
    [0.222, 0.906, 0.844],
    [0.229, 0.911, 0.840],
    [0.237, 0.916, 0.837],
    [0.245, 0.921, 0.834],
    [0.253, 0.926, 0.830],
    [0.261, 0.930, 0.827],
    [0.269, 0.935, 0.823],
    [0.276, 0.939, 0.820],
    [0.284, 0.943, 0.816],
    [0.292, 0.947, 0.813],
    [0.300, 0.951, 0.809],
    [0.308, 0.955, 0.805],
    [0.316, 0.958, 0.802],
    [0.324, 0.962, 0.798],
    [0.331, 0.965, 0.794],
    [0.339, 0.968, 0.791],
    [0.347, 0.971, 0.787],
    [0.355, 0.974, 0.783],
    [0.363, 0.977, 0.779],
    [0.371, 0.979, 0.775],
    [0.378, 0.982, 0.771],
    [0.386, 0.984, 0.767],
    [0.394, 0.986, 0.763],
    [0.402, 0.988, 0.759],
    [0.410, 0.990, 0.755],
    [0.418, 0.992, 0.751],
    [0.425, 0.993, 0.747],
    [0.433, 0.995, 0.743],
    [0.441, 0.996, 0.739],
    [0.449, 0.997, 0.735],
    [0.457, 0.998, 0.731],
    [0.465, 0.998, 0.726],
    [0.473, 0.999, 0.722],
    [0.480, 1.000, 0.718],
    [0.488, 1.000, 0.714],
    [0.496, 1.000, 0.709],
    [0.504, 1.000, 0.705],
    [0.512, 1.000, 0.701],
    [0.520, 1.000, 0.696],
    [0.527, 0.999, 0.692],
    [0.535, 0.998, 0.687],
    [0.543, 0.998, 0.683],
    [0.551, 0.997, 0.678],
    [0.559, 0.996, 0.674],
    [0.567, 0.995, 0.669],
    [0.575, 0.993, 0.665],
    [0.582, 0.992, 0.660],
    [0.590, 0.990, 0.655],
    [0.598, 0.988, 0.651],
    [0.606, 0.986, 0.646],
    [0.614, 0.984, 0.641],
    [0.622, 0.982, 0.636],
    [0.629, 0.979, 0.632],
    [0.637, 0.977, 0.627],
    [0.645, 0.974, 0.622],
    [0.653, 0.971, 0.617],
    [0.661, 0.968, 0.612],
    [0.669, 0.965, 0.608],
    [0.676, 0.962, 0.603],
    [0.684, 0.958, 0.598],
    [0.692, 0.955, 0.593],
    [0.700, 0.951, 0.588],
    [0.708, 0.947, 0.583],
    [0.716, 0.943, 0.578],
    [0.724, 0.939, 0.573],
    [0.731, 0.935, 0.568],
    [0.739, 0.930, 0.563],
    [0.747, 0.926, 0.557],
    [0.755, 0.921, 0.552],
    [0.763, 0.916, 0.547],
    [0.771, 0.911, 0.542],
    [0.778, 0.906, 0.537],
    [0.786, 0.901, 0.532],
    [0.794, 0.895, 0.526],
    [0.802, 0.890, 0.521],
    [0.810, 0.884, 0.516],
    [0.818, 0.878, 0.511],
    [0.825, 0.872, 0.505],
    [0.833, 0.866, 0.500],
    [0.841, 0.860, 0.495],
    [0.849, 0.853, 0.489],
    [0.857, 0.847, 0.484],
    [0.865, 0.840, 0.479],
    [0.873, 0.834, 0.473],
    [0.880, 0.827, 0.468],
    [0.888, 0.820, 0.462],
    [0.896, 0.813, 0.457],
    [0.904, 0.805, 0.451],
    [0.912, 0.798, 0.446],
    [0.920, 0.791, 0.440],
    [0.927, 0.783, 0.435],
    [0.935, 0.775, 0.429],
    [0.943, 0.767, 0.424],
    [0.951, 0.759, 0.418],
    [0.959, 0.751, 0.412],
    [0.967, 0.743, 0.407],
    [0.975, 0.735, 0.401],
    [0.982, 0.726, 0.395],
    [0.990, 0.718, 0.390],
    [0.998, 0.709, 0.384],
    [1.000, 0.701, 0.378],
    [1.000, 0.692, 0.373],
    [1.000, 0.683, 0.367],
    [1.000, 0.674, 0.361],
    [1.000, 0.665, 0.355],
    [1.000, 0.655, 0.350],
    [1.000, 0.646, 0.344],
    [1.000, 0.636, 0.338],
    [1.000, 0.627, 0.332],
    [1.000, 0.617, 0.327],
    [1.000, 0.608, 0.321],
    [1.000, 0.598, 0.315],
    [1.000, 0.588, 0.309],
    [1.000, 0.578, 0.303],
    [1.000, 0.568, 0.297],
    [1.000, 0.557, 0.291],
    [1.000, 0.547, 0.285],
    [1.000, 0.537, 0.280],
    [1.000, 0.526, 0.274],
    [1.000, 0.516, 0.268],
    [1.000, 0.505, 0.262],
    [1.000, 0.495, 0.256],
    [1.000, 0.484, 0.250],
    [1.000, 0.473, 0.244],
    [1.000, 0.462, 0.238],
    [1.000, 0.451, 0.232],
    [1.000, 0.440, 0.226],
    [1.000, 0.429, 0.220],
    [1.000, 0.418, 0.214],
    [1.000, 0.407, 0.208],
    [1.000, 0.395, 0.202],
    [1.000, 0.384, 0.196],
    [1.000, 0.373, 0.190],
    [1.000, 0.361, 0.184],
    [1.000, 0.350, 0.178],
    [1.000, 0.338, 0.172],
    [1.000, 0.327, 0.166],
    [1.000, 0.315, 0.159],
    [1.000, 0.303, 0.153],
    [1.000, 0.291, 0.147],
    [1.000, 0.280, 0.141],
    [1.000, 0.268, 0.135],
    [1.000, 0.256, 0.129],
    [1.000, 0.244, 0.123],
    [1.000, 0.232, 0.117],
    [1.000, 0.220, 0.111],
    [1.000, 0.208, 0.105],
    [1.000, 0.196, 0.098],
    [1.000, 0.184, 0.092],
    [1.000, 0.172, 0.086],
    [1.000, 0.159, 0.080],
    [1.000, 0.147, 0.074],
    [1.000, 0.135, 0.068],
    [1.000, 0.123, 0.062],
    [1.000, 0.111, 0.055],
    [1.000, 0.098, 0.049],
    [1.000, 0.086, 0.043],
    [1.000, 0.074, 0.037],
    [1.000, 0.062, 0.031],
    [1.000, 0.049, 0.025],
    [1.000, 0.037, 0.018],
    [1.000, 0.025, 0.012],
    [1.000, 0.012, 0.006],
    [1.000, 0.000, 0.000],
];

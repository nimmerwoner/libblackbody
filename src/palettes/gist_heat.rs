pub const GIST_HEAT: [[f32; 3]; 256] = [
    [0.000, 0.000, 0.000],
    [0.006, 0.000, 0.000],
    [0.012, 0.000, 0.000],
    [0.018, 0.000, 0.000],
    [0.024, 0.000, 0.000],
    [0.029, 0.000, 0.000],
    [0.035, 0.000, 0.000],
    [0.041, 0.000, 0.000],
    [0.047, 0.000, 0.000],
    [0.053, 0.000, 0.000],
    [0.059, 0.000, 0.000],
    [0.065, 0.000, 0.000],
    [0.071, 0.000, 0.000],
    [0.076, 0.000, 0.000],
    [0.082, 0.000, 0.000],
    [0.088, 0.000, 0.000],
    [0.094, 0.000, 0.000],
    [0.100, 0.000, 0.000],
    [0.106, 0.000, 0.000],
    [0.112, 0.000, 0.000],
    [0.118, 0.000, 0.000],
    [0.124, 0.000, 0.000],
    [0.129, 0.000, 0.000],
    [0.135, 0.000, 0.000],
    [0.141, 0.000, 0.000],
    [0.147, 0.000, 0.000],
    [0.153, 0.000, 0.000],
    [0.159, 0.000, 0.000],
    [0.165, 0.000, 0.000],
    [0.171, 0.000, 0.000],
    [0.176, 0.000, 0.000],
    [0.182, 0.000, 0.000],
    [0.188, 0.000, 0.000],
    [0.194, 0.000, 0.000],
    [0.200, 0.000, 0.000],
    [0.206, 0.000, 0.000],
    [0.212, 0.000, 0.000],
    [0.218, 0.000, 0.000],
    [0.224, 0.000, 0.000],
    [0.229, 0.000, 0.000],
    [0.235, 0.000, 0.000],
    [0.241, 0.000, 0.000],
    [0.247, 0.000, 0.000],
    [0.253, 0.000, 0.000],
    [0.259, 0.000, 0.000],
    [0.265, 0.000, 0.000],
    [0.271, 0.000, 0.000],
    [0.276, 0.000, 0.000],
    [0.282, 0.000, 0.000],
    [0.288, 0.000, 0.000],
    [0.294, 0.000, 0.000],
    [0.300, 0.000, 0.000],
    [0.306, 0.000, 0.000],
    [0.312, 0.000, 0.000],
    [0.318, 0.000, 0.000],
    [0.324, 0.000, 0.000],
    [0.329, 0.000, 0.000],
    [0.335, 0.000, 0.000],
    [0.341, 0.000, 0.000],
    [0.347, 0.000, 0.000],
    [0.353, 0.000, 0.000],
    [0.359, 0.000, 0.000],
    [0.365, 0.000, 0.000],
    [0.371, 0.000, 0.000],
    [0.376, 0.000, 0.000],
    [0.382, 0.000, 0.000],
    [0.388, 0.000, 0.000],
    [0.394, 0.000, 0.000],
    [0.400, 0.000, 0.000],
    [0.406, 0.000, 0.000],
    [0.412, 0.000, 0.000],
    [0.418, 0.000, 0.000],
    [0.424, 0.000, 0.000],
    [0.429, 0.000, 0.000],
    [0.435, 0.000, 0.000],
    [0.441, 0.000, 0.000],
    [0.447, 0.000, 0.000],
    [0.453, 0.000, 0.000],
    [0.459, 0.000, 0.000],
    [0.465, 0.000, 0.000],
    [0.471, 0.000, 0.000],
    [0.476, 0.000, 0.000],
    [0.482, 0.000, 0.000],
    [0.488, 0.000, 0.000],
    [0.494, 0.000, 0.000],
    [0.500, 0.000, 0.000],
    [0.506, 0.000, 0.000],
    [0.512, 0.000, 0.000],
    [0.518, 0.000, 0.000],
    [0.524, 0.000, 0.000],
    [0.529, 0.000, 0.000],
    [0.535, 0.000, 0.000],
    [0.541, 0.000, 0.000],
    [0.547, 0.000, 0.000],
    [0.553, 0.000, 0.000],
    [0.559, 0.000, 0.000],
    [0.565, 0.000, 0.000],
    [0.571, 0.000, 0.000],
    [0.576, 0.000, 0.000],
    [0.582, 0.000, 0.000],
    [0.588, 0.000, 0.000],
    [0.594, 0.000, 0.000],
    [0.600, 0.000, 0.000],
    [0.606, 0.000, 0.000],
    [0.612, 0.000, 0.000],
    [0.618, 0.000, 0.000],
    [0.624, 0.000, 0.000],
    [0.629, 0.000, 0.000],
    [0.635, 0.000, 0.000],
    [0.641, 0.000, 0.000],
    [0.647, 0.000, 0.000],
    [0.653, 0.000, 0.000],
    [0.659, 0.000, 0.000],
    [0.665, 0.000, 0.000],
    [0.671, 0.000, 0.000],
    [0.676, 0.000, 0.000],
    [0.682, 0.000, 0.000],
    [0.688, 0.000, 0.000],
    [0.694, 0.000, 0.000],
    [0.700, 0.000, 0.000],
    [0.706, 0.000, 0.000],
    [0.712, 0.000, 0.000],
    [0.718, 0.000, 0.000],
    [0.724, 0.000, 0.000],
    [0.729, 0.000, 0.000],
    [0.735, 0.000, 0.000],
    [0.741, 0.000, 0.000],
    [0.747, 0.000, 0.000],
    [0.753, 0.004, 0.000],
    [0.759, 0.012, 0.000],
    [0.765, 0.020, 0.000],
    [0.771, 0.027, 0.000],
    [0.776, 0.035, 0.000],
    [0.782, 0.043, 0.000],
    [0.788, 0.051, 0.000],
    [0.794, 0.059, 0.000],
    [0.800, 0.067, 0.000],
    [0.806, 0.075, 0.000],
    [0.812, 0.082, 0.000],
    [0.818, 0.090, 0.000],
    [0.824, 0.098, 0.000],
    [0.829, 0.106, 0.000],
    [0.835, 0.114, 0.000],
    [0.841, 0.122, 0.000],
    [0.847, 0.129, 0.000],
    [0.853, 0.137, 0.000],
    [0.859, 0.145, 0.000],
    [0.865, 0.153, 0.000],
    [0.871, 0.161, 0.000],
    [0.876, 0.169, 0.000],
    [0.882, 0.176, 0.000],
    [0.888, 0.184, 0.000],
    [0.894, 0.192, 0.000],
    [0.900, 0.200, 0.000],
    [0.906, 0.208, 0.000],
    [0.912, 0.216, 0.000],
    [0.918, 0.224, 0.000],
    [0.924, 0.231, 0.000],
    [0.929, 0.239, 0.000],
    [0.935, 0.247, 0.000],
    [0.941, 0.255, 0.000],
    [0.947, 0.263, 0.000],
    [0.953, 0.271, 0.000],
    [0.959, 0.278, 0.000],
    [0.965, 0.286, 0.000],
    [0.971, 0.294, 0.000],
    [0.976, 0.302, 0.000],
    [0.982, 0.310, 0.000],
    [0.988, 0.318, 0.000],
    [0.994, 0.325, 0.000],
    [1.000, 0.333, 0.000],
    [1.000, 0.341, 0.000],
    [1.000, 0.349, 0.000],
    [1.000, 0.357, 0.000],
    [1.000, 0.365, 0.000],
    [1.000, 0.373, 0.000],
    [1.000, 0.380, 0.000],
    [1.000, 0.388, 0.000],
    [1.000, 0.396, 0.000],
    [1.000, 0.404, 0.000],
    [1.000, 0.412, 0.000],
    [1.000, 0.420, 0.000],
    [1.000, 0.427, 0.000],
    [1.000, 0.435, 0.000],
    [1.000, 0.443, 0.000],
    [1.000, 0.451, 0.000],
    [1.000, 0.459, 0.000],
    [1.000, 0.467, 0.000],
    [1.000, 0.475, 0.000],
    [1.000, 0.482, 0.000],
    [1.000, 0.490, 0.000],
    [1.000, 0.498, 0.000],
    [1.000, 0.506, 0.012],
    [1.000, 0.514, 0.027],
    [1.000, 0.522, 0.043],
    [1.000, 0.529, 0.059],
    [1.000, 0.537, 0.075],
    [1.000, 0.545, 0.090],
    [1.000, 0.553, 0.106],
    [1.000, 0.561, 0.122],
    [1.000, 0.569, 0.137],
    [1.000, 0.576, 0.153],
    [1.000, 0.584, 0.169],
    [1.000, 0.592, 0.184],
    [1.000, 0.600, 0.200],
    [1.000, 0.608, 0.216],
    [1.000, 0.616, 0.231],
    [1.000, 0.624, 0.247],
    [1.000, 0.631, 0.263],
    [1.000, 0.639, 0.278],
    [1.000, 0.647, 0.294],
    [1.000, 0.655, 0.310],
    [1.000, 0.663, 0.325],
    [1.000, 0.671, 0.341],
    [1.000, 0.678, 0.357],
    [1.000, 0.686, 0.373],
    [1.000, 0.694, 0.388],
    [1.000, 0.702, 0.404],
    [1.000, 0.710, 0.420],
    [1.000, 0.718, 0.435],
    [1.000, 0.725, 0.451],
    [1.000, 0.733, 0.467],
    [1.000, 0.741, 0.482],
    [1.000, 0.749, 0.498],
    [1.000, 0.757, 0.514],
    [1.000, 0.765, 0.529],
    [1.000, 0.773, 0.545],
    [1.000, 0.780, 0.561],
    [1.000, 0.788, 0.576],
    [1.000, 0.796, 0.592],
    [1.000, 0.804, 0.608],
    [1.000, 0.812, 0.624],
    [1.000, 0.820, 0.639],
    [1.000, 0.827, 0.655],
    [1.000, 0.835, 0.671],
    [1.000, 0.843, 0.686],
    [1.000, 0.851, 0.702],
    [1.000, 0.859, 0.718],
    [1.000, 0.867, 0.733],
    [1.000, 0.875, 0.749],
    [1.000, 0.882, 0.765],
    [1.000, 0.890, 0.780],
    [1.000, 0.898, 0.796],
    [1.000, 0.906, 0.812],
    [1.000, 0.914, 0.827],
    [1.000, 0.922, 0.843],
    [1.000, 0.929, 0.859],
    [1.000, 0.937, 0.875],
    [1.000, 0.945, 0.890],
    [1.000, 0.953, 0.906],
    [1.000, 0.961, 0.922],
    [1.000, 0.969, 0.937],
    [1.000, 0.976, 0.953],
    [1.000, 0.984, 0.969],
    [1.000, 0.992, 0.984],
    [1.000, 1.000, 1.000],
];

pub const GNBU: [[f32; 3]; 256] = [
    [0.969, 0.988, 0.941],
    [0.966, 0.987, 0.939],
    [0.963, 0.986, 0.936],
    [0.960, 0.985, 0.933],
    [0.957, 0.984, 0.931],
    [0.954, 0.983, 0.928],
    [0.952, 0.982, 0.926],
    [0.949, 0.980, 0.923],
    [0.946, 0.979, 0.921],
    [0.943, 0.978, 0.918],
    [0.940, 0.977, 0.915],
    [0.938, 0.976, 0.913],
    [0.935, 0.975, 0.910],
    [0.932, 0.974, 0.908],
    [0.929, 0.973, 0.905],
    [0.926, 0.972, 0.902],
    [0.923, 0.971, 0.900],
    [0.921, 0.969, 0.897],
    [0.918, 0.968, 0.895],
    [0.915, 0.967, 0.892],
    [0.912, 0.966, 0.890],
    [0.909, 0.965, 0.887],
    [0.906, 0.964, 0.884],
    [0.904, 0.963, 0.882],
    [0.901, 0.962, 0.879],
    [0.898, 0.961, 0.877],
    [0.895, 0.959, 0.874],
    [0.892, 0.958, 0.871],
    [0.889, 0.957, 0.869],
    [0.887, 0.956, 0.866],
    [0.884, 0.955, 0.864],
    [0.881, 0.954, 0.861],
    [0.878, 0.953, 0.858],
    [0.876, 0.952, 0.856],
    [0.873, 0.951, 0.853],
    [0.871, 0.950, 0.850],
    [0.868, 0.949, 0.848],
    [0.866, 0.948, 0.845],
    [0.863, 0.947, 0.842],
    [0.861, 0.946, 0.840],
    [0.858, 0.945, 0.837],
    [0.856, 0.944, 0.834],
    [0.854, 0.943, 0.831],
    [0.851, 0.942, 0.829],
    [0.849, 0.941, 0.826],
    [0.846, 0.940, 0.823],
    [0.844, 0.939, 0.821],
    [0.841, 0.938, 0.818],
    [0.839, 0.937, 0.815],
    [0.836, 0.936, 0.812],
    [0.834, 0.935, 0.810],
    [0.831, 0.934, 0.807],
    [0.829, 0.933, 0.804],
    [0.826, 0.932, 0.802],
    [0.824, 0.931, 0.799],
    [0.822, 0.930, 0.796],
    [0.819, 0.929, 0.794],
    [0.817, 0.928, 0.791],
    [0.814, 0.927, 0.788],
    [0.812, 0.926, 0.785],
    [0.809, 0.925, 0.783],
    [0.807, 0.924, 0.780],
    [0.804, 0.923, 0.777],
    [0.802, 0.922, 0.775],
    [0.799, 0.921, 0.772],
    [0.794, 0.919, 0.770],
    [0.790, 0.918, 0.768],
    [0.786, 0.916, 0.766],
    [0.781, 0.914, 0.764],
    [0.777, 0.913, 0.762],
    [0.772, 0.911, 0.760],
    [0.768, 0.909, 0.758],
    [0.763, 0.907, 0.756],
    [0.759, 0.906, 0.754],
    [0.755, 0.904, 0.752],
    [0.750, 0.902, 0.750],
    [0.746, 0.900, 0.748],
    [0.741, 0.899, 0.746],
    [0.737, 0.897, 0.744],
    [0.732, 0.895, 0.743],
    [0.728, 0.894, 0.741],
    [0.724, 0.892, 0.739],
    [0.719, 0.890, 0.737],
    [0.715, 0.888, 0.735],
    [0.710, 0.887, 0.733],
    [0.706, 0.885, 0.731],
    [0.701, 0.883, 0.729],
    [0.697, 0.882, 0.727],
    [0.693, 0.880, 0.725],
    [0.688, 0.878, 0.723],
    [0.684, 0.876, 0.721],
    [0.679, 0.875, 0.719],
    [0.675, 0.873, 0.717],
    [0.670, 0.871, 0.715],
    [0.666, 0.869, 0.713],
    [0.662, 0.868, 0.711],
    [0.657, 0.866, 0.710],
    [0.651, 0.864, 0.712],
    [0.646, 0.862, 0.714],
    [0.640, 0.860, 0.716],
    [0.635, 0.858, 0.718],
    [0.629, 0.855, 0.720],
    [0.624, 0.853, 0.722],
    [0.618, 0.851, 0.723],
    [0.612, 0.849, 0.725],
    [0.607, 0.847, 0.727],
    [0.601, 0.845, 0.729],
    [0.596, 0.843, 0.731],
    [0.590, 0.841, 0.733],
    [0.585, 0.839, 0.734],
    [0.579, 0.837, 0.736],
    [0.574, 0.835, 0.738],
    [0.568, 0.832, 0.740],
    [0.563, 0.830, 0.742],
    [0.557, 0.828, 0.744],
    [0.552, 0.826, 0.746],
    [0.546, 0.824, 0.747],
    [0.540, 0.822, 0.749],
    [0.535, 0.820, 0.751],
    [0.529, 0.818, 0.753],
    [0.524, 0.816, 0.755],
    [0.518, 0.814, 0.757],
    [0.513, 0.812, 0.758],
    [0.507, 0.809, 0.760],
    [0.502, 0.807, 0.762],
    [0.496, 0.805, 0.764],
    [0.491, 0.803, 0.766],
    [0.485, 0.801, 0.768],
    [0.480, 0.798, 0.770],
    [0.474, 0.795, 0.771],
    [0.469, 0.792, 0.773],
    [0.463, 0.789, 0.775],
    [0.457, 0.786, 0.777],
    [0.452, 0.783, 0.779],
    [0.446, 0.780, 0.781],
    [0.441, 0.777, 0.782],
    [0.435, 0.774, 0.784],
    [0.430, 0.771, 0.786],
    [0.424, 0.768, 0.788],
    [0.419, 0.765, 0.790],
    [0.413, 0.762, 0.792],
    [0.408, 0.758, 0.794],
    [0.402, 0.755, 0.795],
    [0.397, 0.752, 0.797],
    [0.391, 0.749, 0.799],
    [0.385, 0.746, 0.801],
    [0.380, 0.743, 0.803],
    [0.374, 0.740, 0.805],
    [0.369, 0.737, 0.806],
    [0.363, 0.734, 0.808],
    [0.358, 0.731, 0.810],
    [0.352, 0.728, 0.812],
    [0.347, 0.725, 0.814],
    [0.341, 0.722, 0.816],
    [0.336, 0.718, 0.818],
    [0.330, 0.715, 0.819],
    [0.325, 0.712, 0.821],
    [0.319, 0.709, 0.823],
    [0.313, 0.706, 0.825],
    [0.308, 0.703, 0.827],
    [0.303, 0.699, 0.826],
    [0.299, 0.694, 0.823],
    [0.295, 0.689, 0.821],
    [0.290, 0.685, 0.818],
    [0.286, 0.680, 0.816],
    [0.282, 0.675, 0.813],
    [0.277, 0.670, 0.810],
    [0.273, 0.665, 0.808],
    [0.269, 0.661, 0.805],
    [0.264, 0.656, 0.803],
    [0.260, 0.651, 0.800],
    [0.256, 0.646, 0.797],
    [0.252, 0.641, 0.795],
    [0.247, 0.637, 0.792],
    [0.243, 0.632, 0.790],
    [0.239, 0.627, 0.787],
    [0.234, 0.622, 0.784],
    [0.230, 0.617, 0.782],
    [0.226, 0.613, 0.779],
    [0.221, 0.608, 0.777],
    [0.217, 0.603, 0.774],
    [0.213, 0.598, 0.772],
    [0.208, 0.593, 0.769],
    [0.204, 0.589, 0.766],
    [0.200, 0.584, 0.764],
    [0.196, 0.579, 0.761],
    [0.191, 0.574, 0.759],
    [0.187, 0.569, 0.756],
    [0.183, 0.565, 0.753],
    [0.178, 0.560, 0.751],
    [0.174, 0.555, 0.748],
    [0.170, 0.550, 0.746],
    [0.165, 0.546, 0.743],
    [0.161, 0.541, 0.741],
    [0.157, 0.537, 0.739],
    [0.152, 0.532, 0.737],
    [0.148, 0.528, 0.735],
    [0.144, 0.524, 0.732],
    [0.140, 0.519, 0.730],
    [0.135, 0.515, 0.728],
    [0.131, 0.510, 0.726],
    [0.127, 0.506, 0.724],
    [0.122, 0.501, 0.721],
    [0.118, 0.497, 0.719],
    [0.114, 0.493, 0.717],
    [0.109, 0.488, 0.715],
    [0.105, 0.484, 0.712],
    [0.101, 0.479, 0.710],
    [0.097, 0.475, 0.708],
    [0.092, 0.470, 0.706],
    [0.088, 0.466, 0.704],
    [0.084, 0.462, 0.701],
    [0.079, 0.457, 0.699],
    [0.075, 0.453, 0.697],
    [0.071, 0.448, 0.695],
    [0.066, 0.444, 0.693],
    [0.062, 0.439, 0.690],
    [0.058, 0.435, 0.688],
    [0.053, 0.431, 0.686],
    [0.049, 0.426, 0.684],
    [0.045, 0.422, 0.681],
    [0.041, 0.417, 0.679],
    [0.036, 0.413, 0.677],
    [0.032, 0.408, 0.675],
    [0.031, 0.404, 0.670],
    [0.031, 0.399, 0.665],
    [0.031, 0.394, 0.659],
    [0.031, 0.389, 0.654],
    [0.031, 0.384, 0.649],
    [0.031, 0.379, 0.643],
    [0.031, 0.374, 0.638],
    [0.031, 0.369, 0.633],
    [0.031, 0.364, 0.628],
    [0.031, 0.359, 0.622],
    [0.031, 0.354, 0.617],
    [0.031, 0.349, 0.612],
    [0.031, 0.344, 0.606],
    [0.031, 0.340, 0.601],
    [0.031, 0.335, 0.596],
    [0.031, 0.330, 0.591],
    [0.031, 0.325, 0.585],
    [0.031, 0.320, 0.580],
    [0.031, 0.315, 0.575],
    [0.031, 0.310, 0.569],
    [0.031, 0.305, 0.564],
    [0.031, 0.300, 0.559],
    [0.031, 0.295, 0.553],
    [0.031, 0.290, 0.548],
    [0.031, 0.285, 0.543],
    [0.031, 0.281, 0.538],
    [0.031, 0.276, 0.532],
    [0.031, 0.271, 0.527],
    [0.031, 0.266, 0.522],
    [0.031, 0.261, 0.516],
    [0.031, 0.256, 0.511],
    [0.031, 0.251, 0.506],
];

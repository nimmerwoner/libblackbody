pub const PUOR: [[f32; 3]; 256] = [
    [0.498, 0.231, 0.031],
    [0.506, 0.236, 0.031],
    [0.514, 0.240, 0.031],
    [0.522, 0.245, 0.030],
    [0.530, 0.249, 0.030],
    [0.538, 0.254, 0.030],
    [0.546, 0.258, 0.030],
    [0.554, 0.263, 0.029],
    [0.562, 0.267, 0.029],
    [0.570, 0.272, 0.029],
    [0.578, 0.276, 0.028],
    [0.586, 0.280, 0.028],
    [0.594, 0.285, 0.028],
    [0.602, 0.289, 0.027],
    [0.610, 0.294, 0.027],
    [0.618, 0.298, 0.027],
    [0.626, 0.303, 0.026],
    [0.634, 0.307, 0.026],
    [0.642, 0.312, 0.026],
    [0.650, 0.316, 0.026],
    [0.658, 0.321, 0.025],
    [0.666, 0.325, 0.025],
    [0.674, 0.329, 0.025],
    [0.682, 0.334, 0.024],
    [0.690, 0.338, 0.024],
    [0.698, 0.343, 0.024],
    [0.705, 0.348, 0.025],
    [0.712, 0.355, 0.027],
    [0.719, 0.361, 0.029],
    [0.726, 0.368, 0.031],
    [0.733, 0.374, 0.033],
    [0.740, 0.381, 0.035],
    [0.747, 0.387, 0.038],
    [0.754, 0.394, 0.040],
    [0.761, 0.400, 0.042],
    [0.768, 0.406, 0.044],
    [0.775, 0.413, 0.046],
    [0.782, 0.419, 0.048],
    [0.788, 0.426, 0.050],
    [0.795, 0.432, 0.053],
    [0.802, 0.439, 0.055],
    [0.809, 0.445, 0.057],
    [0.816, 0.452, 0.059],
    [0.823, 0.458, 0.061],
    [0.830, 0.465, 0.063],
    [0.837, 0.471, 0.066],
    [0.844, 0.478, 0.068],
    [0.851, 0.484, 0.070],
    [0.858, 0.490, 0.072],
    [0.865, 0.497, 0.074],
    [0.872, 0.503, 0.076],
    [0.878, 0.510, 0.078],
    [0.883, 0.518, 0.091],
    [0.887, 0.526, 0.103],
    [0.892, 0.535, 0.115],
    [0.896, 0.543, 0.127],
    [0.901, 0.551, 0.139],
    [0.905, 0.560, 0.151],
    [0.910, 0.568, 0.163],
    [0.914, 0.576, 0.176],
    [0.919, 0.585, 0.188],
    [0.923, 0.593, 0.200],
    [0.927, 0.601, 0.212],
    [0.932, 0.609, 0.224],
    [0.936, 0.618, 0.236],
    [0.941, 0.626, 0.249],
    [0.945, 0.634, 0.261],
    [0.950, 0.643, 0.273],
    [0.954, 0.651, 0.285],
    [0.959, 0.659, 0.297],
    [0.963, 0.668, 0.309],
    [0.968, 0.676, 0.321],
    [0.972, 0.684, 0.334],
    [0.977, 0.693, 0.346],
    [0.981, 0.701, 0.358],
    [0.985, 0.709, 0.370],
    [0.990, 0.717, 0.382],
    [0.992, 0.725, 0.395],
    [0.992, 0.731, 0.407],
    [0.993, 0.737, 0.420],
    [0.993, 0.743, 0.433],
    [0.993, 0.749, 0.446],
    [0.993, 0.755, 0.458],
    [0.993, 0.762, 0.471],
    [0.993, 0.768, 0.484],
    [0.993, 0.774, 0.497],
    [0.994, 0.780, 0.509],
    [0.994, 0.786, 0.522],
    [0.994, 0.792, 0.535],
    [0.994, 0.798, 0.548],
    [0.994, 0.805, 0.561],
    [0.994, 0.811, 0.573],
    [0.995, 0.817, 0.586],
    [0.995, 0.823, 0.599],
    [0.995, 0.829, 0.612],
    [0.995, 0.835, 0.624],
    [0.995, 0.842, 0.637],
    [0.995, 0.848, 0.650],
    [0.995, 0.854, 0.663],
    [0.996, 0.860, 0.675],
    [0.996, 0.866, 0.688],
    [0.996, 0.872, 0.701],
    [0.996, 0.878, 0.714],
    [0.995, 0.882, 0.724],
    [0.994, 0.886, 0.734],
    [0.993, 0.889, 0.744],
    [0.992, 0.893, 0.754],
    [0.991, 0.896, 0.764],
    [0.990, 0.900, 0.774],
    [0.989, 0.903, 0.784],
    [0.987, 0.907, 0.794],
    [0.986, 0.910, 0.804],
    [0.985, 0.914, 0.814],
    [0.984, 0.917, 0.824],
    [0.983, 0.921, 0.834],
    [0.982, 0.924, 0.844],
    [0.981, 0.928, 0.854],
    [0.980, 0.931, 0.864],
    [0.979, 0.935, 0.874],
    [0.978, 0.939, 0.884],
    [0.977, 0.942, 0.894],
    [0.976, 0.946, 0.904],
    [0.975, 0.949, 0.914],
    [0.973, 0.953, 0.924],
    [0.972, 0.956, 0.934],
    [0.971, 0.960, 0.944],
    [0.970, 0.963, 0.954],
    [0.969, 0.967, 0.964],
    [0.966, 0.966, 0.968],
    [0.961, 0.962, 0.966],
    [0.957, 0.957, 0.964],
    [0.952, 0.953, 0.962],
    [0.947, 0.949, 0.960],
    [0.942, 0.944, 0.958],
    [0.938, 0.940, 0.957],
    [0.933, 0.935, 0.955],
    [0.928, 0.931, 0.953],
    [0.923, 0.926, 0.951],
    [0.919, 0.922, 0.949],
    [0.914, 0.917, 0.947],
    [0.909, 0.913, 0.946],
    [0.904, 0.908, 0.944],
    [0.900, 0.904, 0.942],
    [0.895, 0.900, 0.940],
    [0.890, 0.895, 0.938],
    [0.885, 0.891, 0.936],
    [0.880, 0.886, 0.934],
    [0.876, 0.882, 0.933],
    [0.871, 0.877, 0.931],
    [0.866, 0.873, 0.929],
    [0.861, 0.868, 0.927],
    [0.857, 0.864, 0.925],
    [0.852, 0.859, 0.923],
    [0.847, 0.855, 0.922],
    [0.841, 0.848, 0.918],
    [0.835, 0.840, 0.914],
    [0.830, 0.833, 0.910],
    [0.824, 0.826, 0.906],
    [0.818, 0.819, 0.902],
    [0.812, 0.812, 0.899],
    [0.806, 0.804, 0.895],
    [0.800, 0.797, 0.891],
    [0.794, 0.790, 0.887],
    [0.789, 0.783, 0.883],
    [0.783, 0.775, 0.879],
    [0.777, 0.768, 0.875],
    [0.771, 0.761, 0.872],
    [0.765, 0.754, 0.868],
    [0.759, 0.746, 0.864],
    [0.754, 0.739, 0.860],
    [0.748, 0.732, 0.856],
    [0.742, 0.725, 0.852],
    [0.736, 0.718, 0.849],
    [0.730, 0.710, 0.845],
    [0.724, 0.703, 0.841],
    [0.718, 0.696, 0.837],
    [0.713, 0.689, 0.833],
    [0.707, 0.681, 0.829],
    [0.701, 0.674, 0.825],
    [0.694, 0.666, 0.821],
    [0.687, 0.658, 0.815],
    [0.679, 0.649, 0.809],
    [0.671, 0.640, 0.803],
    [0.663, 0.632, 0.797],
    [0.656, 0.623, 0.791],
    [0.648, 0.615, 0.786],
    [0.640, 0.606, 0.780],
    [0.633, 0.597, 0.774],
    [0.625, 0.589, 0.768],
    [0.617, 0.580, 0.762],
    [0.610, 0.572, 0.756],
    [0.602, 0.563, 0.750],
    [0.594, 0.554, 0.745],
    [0.587, 0.546, 0.739],
    [0.579, 0.537, 0.733],
    [0.571, 0.528, 0.727],
    [0.563, 0.520, 0.721],
    [0.556, 0.511, 0.715],
    [0.548, 0.503, 0.710],
    [0.540, 0.494, 0.704],
    [0.533, 0.485, 0.698],
    [0.525, 0.477, 0.692],
    [0.517, 0.468, 0.686],
    [0.510, 0.460, 0.680],
    [0.502, 0.451, 0.675],
    [0.495, 0.439, 0.669],
    [0.488, 0.428, 0.663],
    [0.482, 0.416, 0.658],
    [0.475, 0.404, 0.652],
    [0.468, 0.393, 0.647],
    [0.461, 0.381, 0.641],
    [0.455, 0.369, 0.636],
    [0.448, 0.357, 0.630],
    [0.441, 0.346, 0.625],
    [0.434, 0.334, 0.619],
    [0.428, 0.322, 0.614],
    [0.421, 0.311, 0.608],
    [0.414, 0.299, 0.603],
    [0.407, 0.287, 0.597],
    [0.400, 0.276, 0.591],
    [0.394, 0.264, 0.586],
    [0.387, 0.252, 0.580],
    [0.380, 0.241, 0.575],
    [0.373, 0.229, 0.569],
    [0.367, 0.217, 0.564],
    [0.360, 0.206, 0.558],
    [0.353, 0.194, 0.553],
    [0.346, 0.182, 0.547],
    [0.340, 0.170, 0.542],
    [0.333, 0.159, 0.536],
    [0.326, 0.150, 0.529],
    [0.320, 0.144, 0.519],
    [0.314, 0.138, 0.510],
    [0.308, 0.132, 0.500],
    [0.302, 0.126, 0.491],
    [0.296, 0.120, 0.482],
    [0.290, 0.114, 0.472],
    [0.284, 0.108, 0.463],
    [0.278, 0.102, 0.454],
    [0.272, 0.096, 0.444],
    [0.266, 0.090, 0.435],
    [0.260, 0.084, 0.425],
    [0.254, 0.078, 0.416],
    [0.248, 0.072, 0.407],
    [0.242, 0.066, 0.397],
    [0.236, 0.060, 0.388],
    [0.230, 0.054, 0.379],
    [0.224, 0.048, 0.369],
    [0.218, 0.042, 0.360],
    [0.212, 0.036, 0.350],
    [0.206, 0.030, 0.341],
    [0.200, 0.024, 0.332],
    [0.194, 0.018, 0.322],
    [0.188, 0.012, 0.313],
    [0.182, 0.006, 0.303],
    [0.176, 0.000, 0.294],
];

pub const AFMHOT: [[f32; 3]; 256] = [
    [0.000, 0.000, 0.000],
    [0.008, 0.000, 0.000],
    [0.016, 0.000, 0.000],
    [0.024, 0.000, 0.000],
    [0.031, 0.000, 0.000],
    [0.039, 0.000, 0.000],
    [0.047, 0.000, 0.000],
    [0.055, 0.000, 0.000],
    [0.063, 0.000, 0.000],
    [0.071, 0.000, 0.000],
    [0.078, 0.000, 0.000],
    [0.086, 0.000, 0.000],
    [0.094, 0.000, 0.000],
    [0.102, 0.000, 0.000],
    [0.110, 0.000, 0.000],
    [0.118, 0.000, 0.000],
    [0.125, 0.000, 0.000],
    [0.133, 0.000, 0.000],
    [0.141, 0.000, 0.000],
    [0.149, 0.000, 0.000],
    [0.157, 0.000, 0.000],
    [0.165, 0.000, 0.000],
    [0.173, 0.000, 0.000],
    [0.180, 0.000, 0.000],
    [0.188, 0.000, 0.000],
    [0.196, 0.000, 0.000],
    [0.204, 0.000, 0.000],
    [0.212, 0.000, 0.000],
    [0.220, 0.000, 0.000],
    [0.227, 0.000, 0.000],
    [0.235, 0.000, 0.000],
    [0.243, 0.000, 0.000],
    [0.251, 0.000, 0.000],
    [0.259, 0.000, 0.000],
    [0.267, 0.000, 0.000],
    [0.275, 0.000, 0.000],
    [0.282, 0.000, 0.000],
    [0.290, 0.000, 0.000],
    [0.298, 0.000, 0.000],
    [0.306, 0.000, 0.000],
    [0.314, 0.000, 0.000],
    [0.322, 0.000, 0.000],
    [0.329, 0.000, 0.000],
    [0.337, 0.000, 0.000],
    [0.345, 0.000, 0.000],
    [0.353, 0.000, 0.000],
    [0.361, 0.000, 0.000],
    [0.369, 0.000, 0.000],
    [0.376, 0.000, 0.000],
    [0.384, 0.000, 0.000],
    [0.392, 0.000, 0.000],
    [0.400, 0.000, 0.000],
    [0.408, 0.000, 0.000],
    [0.416, 0.000, 0.000],
    [0.424, 0.000, 0.000],
    [0.431, 0.000, 0.000],
    [0.439, 0.000, 0.000],
    [0.447, 0.000, 0.000],
    [0.455, 0.000, 0.000],
    [0.463, 0.000, 0.000],
    [0.471, 0.000, 0.000],
    [0.478, 0.000, 0.000],
    [0.486, 0.000, 0.000],
    [0.494, 0.000, 0.000],
    [0.502, 0.002, 0.000],
    [0.510, 0.010, 0.000],
    [0.518, 0.018, 0.000],
    [0.525, 0.025, 0.000],
    [0.533, 0.033, 0.000],
    [0.541, 0.041, 0.000],
    [0.549, 0.049, 0.000],
    [0.557, 0.057, 0.000],
    [0.565, 0.065, 0.000],
    [0.573, 0.073, 0.000],
    [0.580, 0.080, 0.000],
    [0.588, 0.088, 0.000],
    [0.596, 0.096, 0.000],
    [0.604, 0.104, 0.000],
    [0.612, 0.112, 0.000],
    [0.620, 0.120, 0.000],
    [0.627, 0.127, 0.000],
    [0.635, 0.135, 0.000],
    [0.643, 0.143, 0.000],
    [0.651, 0.151, 0.000],
    [0.659, 0.159, 0.000],
    [0.667, 0.167, 0.000],
    [0.675, 0.175, 0.000],
    [0.682, 0.182, 0.000],
    [0.690, 0.190, 0.000],
    [0.698, 0.198, 0.000],
    [0.706, 0.206, 0.000],
    [0.714, 0.214, 0.000],
    [0.722, 0.222, 0.000],
    [0.729, 0.229, 0.000],
    [0.737, 0.237, 0.000],
    [0.745, 0.245, 0.000],
    [0.753, 0.253, 0.000],
    [0.761, 0.261, 0.000],
    [0.769, 0.269, 0.000],
    [0.776, 0.276, 0.000],
    [0.784, 0.284, 0.000],
    [0.792, 0.292, 0.000],
    [0.800, 0.300, 0.000],
    [0.808, 0.308, 0.000],
    [0.816, 0.316, 0.000],
    [0.824, 0.324, 0.000],
    [0.831, 0.331, 0.000],
    [0.839, 0.339, 0.000],
    [0.847, 0.347, 0.000],
    [0.855, 0.355, 0.000],
    [0.863, 0.363, 0.000],
    [0.871, 0.371, 0.000],
    [0.878, 0.378, 0.000],
    [0.886, 0.386, 0.000],
    [0.894, 0.394, 0.000],
    [0.902, 0.402, 0.000],
    [0.910, 0.410, 0.000],
    [0.918, 0.418, 0.000],
    [0.925, 0.425, 0.000],
    [0.933, 0.433, 0.000],
    [0.941, 0.441, 0.000],
    [0.949, 0.449, 0.000],
    [0.957, 0.457, 0.000],
    [0.965, 0.465, 0.000],
    [0.973, 0.473, 0.000],
    [0.980, 0.480, 0.000],
    [0.988, 0.488, 0.000],
    [0.996, 0.496, 0.000],
    [1.000, 0.504, 0.004],
    [1.000, 0.512, 0.012],
    [1.000, 0.520, 0.020],
    [1.000, 0.527, 0.027],
    [1.000, 0.535, 0.035],
    [1.000, 0.543, 0.043],
    [1.000, 0.551, 0.051],
    [1.000, 0.559, 0.059],
    [1.000, 0.567, 0.067],
    [1.000, 0.575, 0.075],
    [1.000, 0.582, 0.082],
    [1.000, 0.590, 0.090],
    [1.000, 0.598, 0.098],
    [1.000, 0.606, 0.106],
    [1.000, 0.614, 0.114],
    [1.000, 0.622, 0.122],
    [1.000, 0.629, 0.129],
    [1.000, 0.637, 0.137],
    [1.000, 0.645, 0.145],
    [1.000, 0.653, 0.153],
    [1.000, 0.661, 0.161],
    [1.000, 0.669, 0.169],
    [1.000, 0.676, 0.176],
    [1.000, 0.684, 0.184],
    [1.000, 0.692, 0.192],
    [1.000, 0.700, 0.200],
    [1.000, 0.708, 0.208],
    [1.000, 0.716, 0.216],
    [1.000, 0.724, 0.224],
    [1.000, 0.731, 0.231],
    [1.000, 0.739, 0.239],
    [1.000, 0.747, 0.247],
    [1.000, 0.755, 0.255],
    [1.000, 0.763, 0.263],
    [1.000, 0.771, 0.271],
    [1.000, 0.778, 0.278],
    [1.000, 0.786, 0.286],
    [1.000, 0.794, 0.294],
    [1.000, 0.802, 0.302],
    [1.000, 0.810, 0.310],
    [1.000, 0.818, 0.318],
    [1.000, 0.825, 0.325],
    [1.000, 0.833, 0.333],
    [1.000, 0.841, 0.341],
    [1.000, 0.849, 0.349],
    [1.000, 0.857, 0.357],
    [1.000, 0.865, 0.365],
    [1.000, 0.873, 0.373],
    [1.000, 0.880, 0.380],
    [1.000, 0.888, 0.388],
    [1.000, 0.896, 0.396],
    [1.000, 0.904, 0.404],
    [1.000, 0.912, 0.412],
    [1.000, 0.920, 0.420],
    [1.000, 0.927, 0.427],
    [1.000, 0.935, 0.435],
    [1.000, 0.943, 0.443],
    [1.000, 0.951, 0.451],
    [1.000, 0.959, 0.459],
    [1.000, 0.967, 0.467],
    [1.000, 0.975, 0.475],
    [1.000, 0.982, 0.482],
    [1.000, 0.990, 0.490],
    [1.000, 0.998, 0.498],
    [1.000, 1.000, 0.506],
    [1.000, 1.000, 0.514],
    [1.000, 1.000, 0.522],
    [1.000, 1.000, 0.529],
    [1.000, 1.000, 0.537],
    [1.000, 1.000, 0.545],
    [1.000, 1.000, 0.553],
    [1.000, 1.000, 0.561],
    [1.000, 1.000, 0.569],
    [1.000, 1.000, 0.576],
    [1.000, 1.000, 0.584],
    [1.000, 1.000, 0.592],
    [1.000, 1.000, 0.600],
    [1.000, 1.000, 0.608],
    [1.000, 1.000, 0.616],
    [1.000, 1.000, 0.624],
    [1.000, 1.000, 0.631],
    [1.000, 1.000, 0.639],
    [1.000, 1.000, 0.647],
    [1.000, 1.000, 0.655],
    [1.000, 1.000, 0.663],
    [1.000, 1.000, 0.671],
    [1.000, 1.000, 0.678],
    [1.000, 1.000, 0.686],
    [1.000, 1.000, 0.694],
    [1.000, 1.000, 0.702],
    [1.000, 1.000, 0.710],
    [1.000, 1.000, 0.718],
    [1.000, 1.000, 0.725],
    [1.000, 1.000, 0.733],
    [1.000, 1.000, 0.741],
    [1.000, 1.000, 0.749],
    [1.000, 1.000, 0.757],
    [1.000, 1.000, 0.765],
    [1.000, 1.000, 0.773],
    [1.000, 1.000, 0.780],
    [1.000, 1.000, 0.788],
    [1.000, 1.000, 0.796],
    [1.000, 1.000, 0.804],
    [1.000, 1.000, 0.812],
    [1.000, 1.000, 0.820],
    [1.000, 1.000, 0.827],
    [1.000, 1.000, 0.835],
    [1.000, 1.000, 0.843],
    [1.000, 1.000, 0.851],
    [1.000, 1.000, 0.859],
    [1.000, 1.000, 0.867],
    [1.000, 1.000, 0.875],
    [1.000, 1.000, 0.882],
    [1.000, 1.000, 0.890],
    [1.000, 1.000, 0.898],
    [1.000, 1.000, 0.906],
    [1.000, 1.000, 0.914],
    [1.000, 1.000, 0.922],
    [1.000, 1.000, 0.929],
    [1.000, 1.000, 0.937],
    [1.000, 1.000, 0.945],
    [1.000, 1.000, 0.953],
    [1.000, 1.000, 0.961],
    [1.000, 1.000, 0.969],
    [1.000, 1.000, 0.976],
    [1.000, 1.000, 0.984],
    [1.000, 1.000, 0.992],
    [1.000, 1.000, 1.000],
];

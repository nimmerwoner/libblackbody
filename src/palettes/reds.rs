pub const REDS: [[f32; 3]; 256] = [
    [1.000, 0.961, 0.941],
    [1.000, 0.958, 0.937],
    [1.000, 0.956, 0.934],
    [1.000, 0.953, 0.930],
    [1.000, 0.950, 0.926],
    [0.999, 0.948, 0.923],
    [0.999, 0.945, 0.919],
    [0.999, 0.943, 0.915],
    [0.999, 0.940, 0.912],
    [0.999, 0.938, 0.908],
    [0.999, 0.935, 0.904],
    [0.999, 0.932, 0.901],
    [0.999, 0.930, 0.897],
    [0.998, 0.927, 0.893],
    [0.998, 0.925, 0.890],
    [0.998, 0.922, 0.886],
    [0.998, 0.919, 0.882],
    [0.998, 0.917, 0.878],
    [0.998, 0.914, 0.875],
    [0.998, 0.912, 0.871],
    [0.998, 0.909, 0.867],
    [0.997, 0.907, 0.864],
    [0.997, 0.904, 0.860],
    [0.997, 0.901, 0.856],
    [0.997, 0.899, 0.853],
    [0.997, 0.896, 0.849],
    [0.997, 0.894, 0.845],
    [0.997, 0.891, 0.842],
    [0.997, 0.888, 0.838],
    [0.996, 0.886, 0.834],
    [0.996, 0.883, 0.830],
    [0.996, 0.881, 0.827],
    [0.996, 0.878, 0.823],
    [0.996, 0.873, 0.817],
    [0.996, 0.869, 0.811],
    [0.995, 0.864, 0.805],
    [0.995, 0.860, 0.799],
    [0.995, 0.855, 0.793],
    [0.995, 0.851, 0.787],
    [0.994, 0.846, 0.781],
    [0.994, 0.841, 0.775],
    [0.994, 0.837, 0.769],
    [0.994, 0.832, 0.762],
    [0.993, 0.828, 0.756],
    [0.993, 0.823, 0.750],
    [0.993, 0.819, 0.744],
    [0.993, 0.814, 0.738],
    [0.992, 0.810, 0.732],
    [0.992, 0.805, 0.726],
    [0.992, 0.800, 0.720],
    [0.992, 0.796, 0.714],
    [0.991, 0.791, 0.708],
    [0.991, 0.787, 0.702],
    [0.991, 0.782, 0.696],
    [0.991, 0.778, 0.690],
    [0.990, 0.773, 0.684],
    [0.990, 0.769, 0.678],
    [0.990, 0.764, 0.672],
    [0.990, 0.760, 0.666],
    [0.989, 0.755, 0.660],
    [0.989, 0.750, 0.654],
    [0.989, 0.746, 0.648],
    [0.989, 0.741, 0.642],
    [0.988, 0.737, 0.636],
    [0.988, 0.732, 0.630],
    [0.988, 0.727, 0.624],
    [0.988, 0.722, 0.618],
    [0.988, 0.717, 0.613],
    [0.988, 0.712, 0.607],
    [0.988, 0.707, 0.601],
    [0.988, 0.702, 0.595],
    [0.988, 0.697, 0.589],
    [0.988, 0.692, 0.584],
    [0.988, 0.687, 0.578],
    [0.988, 0.682, 0.572],
    [0.988, 0.677, 0.566],
    [0.988, 0.672, 0.561],
    [0.988, 0.666, 0.555],
    [0.988, 0.661, 0.549],
    [0.988, 0.656, 0.543],
    [0.988, 0.651, 0.537],
    [0.988, 0.646, 0.532],
    [0.988, 0.641, 0.526],
    [0.988, 0.636, 0.520],
    [0.988, 0.631, 0.514],
    [0.988, 0.626, 0.508],
    [0.988, 0.621, 0.503],
    [0.988, 0.616, 0.497],
    [0.988, 0.611, 0.491],
    [0.988, 0.606, 0.485],
    [0.988, 0.601, 0.480],
    [0.988, 0.596, 0.474],
    [0.988, 0.591, 0.468],
    [0.988, 0.586, 0.462],
    [0.988, 0.581, 0.456],
    [0.988, 0.576, 0.451],
    [0.988, 0.571, 0.445],
    [0.988, 0.566, 0.440],
    [0.988, 0.561, 0.435],
    [0.988, 0.556, 0.430],
    [0.988, 0.551, 0.426],
    [0.988, 0.546, 0.421],
    [0.987, 0.541, 0.416],
    [0.987, 0.536, 0.411],
    [0.987, 0.531, 0.406],
    [0.987, 0.526, 0.401],
    [0.987, 0.521, 0.396],
    [0.987, 0.517, 0.391],
    [0.987, 0.512, 0.386],
    [0.987, 0.507, 0.381],
    [0.986, 0.502, 0.376],
    [0.986, 0.497, 0.371],
    [0.986, 0.492, 0.366],
    [0.986, 0.487, 0.362],
    [0.986, 0.482, 0.357],
    [0.986, 0.477, 0.352],
    [0.986, 0.472, 0.347],
    [0.986, 0.467, 0.342],
    [0.985, 0.462, 0.337],
    [0.985, 0.458, 0.332],
    [0.985, 0.453, 0.327],
    [0.985, 0.448, 0.322],
    [0.985, 0.443, 0.317],
    [0.985, 0.438, 0.312],
    [0.985, 0.433, 0.307],
    [0.985, 0.428, 0.302],
    [0.984, 0.423, 0.298],
    [0.984, 0.418, 0.293],
    [0.984, 0.413, 0.288],
    [0.982, 0.407, 0.285],
    [0.981, 0.401, 0.281],
    [0.979, 0.395, 0.277],
    [0.978, 0.390, 0.274],
    [0.976, 0.384, 0.270],
    [0.975, 0.378, 0.266],
    [0.973, 0.372, 0.263],
    [0.972, 0.367, 0.259],
    [0.970, 0.361, 0.255],
    [0.969, 0.355, 0.251],
    [0.967, 0.349, 0.248],
    [0.966, 0.343, 0.244],
    [0.964, 0.338, 0.240],
    [0.963, 0.332, 0.237],
    [0.961, 0.326, 0.233],
    [0.960, 0.320, 0.229],
    [0.958, 0.314, 0.226],
    [0.957, 0.309, 0.222],
    [0.956, 0.303, 0.218],
    [0.954, 0.297, 0.215],
    [0.953, 0.291, 0.211],
    [0.951, 0.286, 0.207],
    [0.950, 0.280, 0.203],
    [0.948, 0.274, 0.200],
    [0.947, 0.268, 0.196],
    [0.945, 0.262, 0.192],
    [0.944, 0.257, 0.189],
    [0.942, 0.251, 0.185],
    [0.941, 0.245, 0.181],
    [0.939, 0.239, 0.178],
    [0.938, 0.234, 0.174],
    [0.934, 0.229, 0.171],
    [0.930, 0.224, 0.170],
    [0.926, 0.220, 0.168],
    [0.921, 0.216, 0.166],
    [0.917, 0.211, 0.164],
    [0.912, 0.207, 0.162],
    [0.908, 0.203, 0.160],
    [0.903, 0.199, 0.158],
    [0.899, 0.194, 0.157],
    [0.895, 0.190, 0.155],
    [0.890, 0.186, 0.153],
    [0.886, 0.181, 0.151],
    [0.881, 0.177, 0.149],
    [0.877, 0.173, 0.147],
    [0.872, 0.168, 0.146],
    [0.868, 0.164, 0.144],
    [0.864, 0.160, 0.142],
    [0.859, 0.155, 0.140],
    [0.855, 0.151, 0.138],
    [0.850, 0.147, 0.136],
    [0.846, 0.143, 0.134],
    [0.841, 0.138, 0.133],
    [0.837, 0.134, 0.131],
    [0.833, 0.130, 0.129],
    [0.828, 0.125, 0.127],
    [0.824, 0.121, 0.125],
    [0.819, 0.117, 0.123],
    [0.815, 0.112, 0.122],
    [0.810, 0.108, 0.120],
    [0.806, 0.104, 0.118],
    [0.802, 0.100, 0.116],
    [0.797, 0.095, 0.114],
    [0.793, 0.093, 0.113],
    [0.788, 0.092, 0.112],
    [0.783, 0.091, 0.111],
    [0.779, 0.090, 0.110],
    [0.774, 0.089, 0.109],
    [0.769, 0.088, 0.108],
    [0.765, 0.087, 0.107],
    [0.760, 0.086, 0.106],
    [0.755, 0.084, 0.105],
    [0.750, 0.083, 0.104],
    [0.746, 0.082, 0.103],
    [0.741, 0.081, 0.102],
    [0.736, 0.080, 0.101],
    [0.732, 0.079, 0.100],
    [0.727, 0.078, 0.099],
    [0.722, 0.077, 0.098],
    [0.718, 0.076, 0.097],
    [0.713, 0.074, 0.096],
    [0.708, 0.073, 0.095],
    [0.704, 0.072, 0.094],
    [0.699, 0.071, 0.093],
    [0.694, 0.070, 0.092],
    [0.690, 0.069, 0.091],
    [0.685, 0.068, 0.090],
    [0.680, 0.067, 0.089],
    [0.676, 0.066, 0.088],
    [0.671, 0.064, 0.087],
    [0.666, 0.063, 0.086],
    [0.662, 0.062, 0.085],
    [0.657, 0.061, 0.084],
    [0.652, 0.060, 0.083],
    [0.648, 0.059, 0.082],
    [0.640, 0.057, 0.081],
    [0.633, 0.055, 0.081],
    [0.625, 0.054, 0.080],
    [0.618, 0.052, 0.079],
    [0.610, 0.050, 0.078],
    [0.602, 0.048, 0.077],
    [0.595, 0.046, 0.076],
    [0.587, 0.044, 0.075],
    [0.579, 0.042, 0.074],
    [0.572, 0.041, 0.073],
    [0.564, 0.039, 0.072],
    [0.556, 0.037, 0.071],
    [0.549, 0.035, 0.070],
    [0.541, 0.033, 0.069],
    [0.534, 0.031, 0.068],
    [0.526, 0.030, 0.067],
    [0.518, 0.028, 0.066],
    [0.511, 0.026, 0.065],
    [0.503, 0.024, 0.064],
    [0.495, 0.022, 0.063],
    [0.488, 0.020, 0.062],
    [0.480, 0.018, 0.061],
    [0.473, 0.017, 0.060],
    [0.465, 0.015, 0.059],
    [0.457, 0.013, 0.058],
    [0.450, 0.011, 0.057],
    [0.442, 0.009, 0.056],
    [0.434, 0.007, 0.055],
    [0.427, 0.006, 0.054],
    [0.419, 0.004, 0.053],
    [0.412, 0.002, 0.052],
    [0.404, 0.000, 0.051],
];

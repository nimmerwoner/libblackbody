# Unreleased
* Rotate thermal data when EXIF field Orientation is set

# 0.5.0 and earlier
* Added support for reading thermal FLIR data
* Added support for reading thermal TIFFs (single-banded ones)
* Added method to render thermal data to several palettes
